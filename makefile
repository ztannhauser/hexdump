
CC = gcc

CFLAGS += -Wall
CFLAGS += -Werror
CFLAGS += -O3

LDFLAGS += 

default: bin/hexdump

bin:
	rm -f ./bin
	ln -s `mktemp -d` bin
	find -type d | sed 's ^ bin/ ' | xargs -d \\n mkdir -p

~/bin:
	mkdir ~/bin

SRCS += hexdump.c

OBJS = $(patsubst %.c,bin/%.o,$(SRCS))

run: bin/hexdump
	valgrind $< ./hexdump.c

install: ~/bin/hexdump

~/bin/hexdump: bin/hexdump | ~/bin bin
	cp -vf $< $@

bin/hexdump: $(OBJS) | bin
	$(CC) $(LDFLAGS) $^ $(LOADLIBES) $(LDLIBS) -o $@

bin/%.o: %.c | bin
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@



